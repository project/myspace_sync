<?php

/**
 * Returns an array of profile info based on profile page HTML.
 */
function myspace_parse_profile($html) {

  $profile = array();
  $regex = '#<a class="userLink"[^>]*>(?P<name>[^<]+)</a>#is';

  if (preg_match($regex, $html, $match)) {

    $profile['name'] = trim($match['name']);

  } // if
  
  $friendcount_regex = '#has <span class="count">(?P<count>[^<]+)</span> friends#is';

  if (preg_match($friendcount_regex, $html, $match)) {

    $profile['friendcount'] = intval(trim($match['count']));

  } // if
  return $profile;

} // myspace_parse_profile

/**
 * Returns an array of event arrays based on events page HTML.
 */
function myspace_parse_events($html) {

  $events = array(); 
  $regex = '#<li class="moduleItem event[^>]*>[^<]*<div class="entryDate[^>]*>[^<]*<span class="month[^>]*>(?P<month>[^<]+)</span>[^<]*<span class="day[^>]*>(?P<day>[^<]+)</span>[^<]*</div>[^<]*<div class="details[^>]*>[^<]*<h4><a href="(?P<url>[^"]+)"[^>]*>(?P<venue>[^<]+)</a></span></h4>[^<]*<p>(?P<city>[^<]+)</p>[^<]*</div>[^<]*</li>#is';
  $vcard_regex = '#<li class="moduleItem event[^>]*>[^<]*<div class="entryDate[^>]*>[^<]*<span class="month[^>]*>(?P<month>[^<]+)</span>[^<]*<span class="day[^>]*>(?P<day>[^<]+)</span>[^<]*</div>[^<]*<div class="details[^>]*>[^<]*<h4><a href="(?P<url>[^"]+)"[^>]*>(?P<venue>[^<]+)</a></h4>[^<]*<p>[^<]*<span class="location[^>]*>[^<]*<span class="adr[^>]*>[^<]*<span class="fn[^>]*>[^<]*</span>[^<]*<span class="locality[^>]*>(?P<city>[^<]+)</span>[^<]*<span class="region[^>]*>(?P<state>[^<]*)</span>[^<]*<span class="country-name[^>]*>(?P<country>[^<]*)</span>[^<]*<span></span>[^<]*</span>[^<]*</span>[^<]*<span class="dtstart">[^<]*<span class="value-title" title="(?P<start>[^"]+)">#is';

  if (preg_match_all($regex, $html, $matches, PREG_SET_ORDER)) {

    foreach ($matches as $match) {

      $date = strtotime($match['month'] . ' ' . $match['day']);
      $location = explode(',' , $match['city']);

      $event = array();

      $event['month'] = intval(date('n', $date));
      $event['day'] = intval(date('j', $date));
      $event['venue'] = trim($match['venue']);
      $event['state'] = trim(array_pop($location));
      $event['city'] = trim(array_pop($location));
      $event['url'] = trim($match['url']);

      if (strpos($event['url'], 'http://www.myspace.com') === FALSE) {
        $event['url'] = 'http://www.myspace.com' . $event['url'];
      } // if

      $events[] = $event;

    } // foreach

  } // if
  elseif (preg_match_all($vcard_regex, $html, $matches, PREG_SET_ORDER)) {

    foreach ($matches as $match) {

      $date = strtotime($match['start']);

      $event = array();

      $event['year'] = intval(date('Y', $date));
      $event['month'] = intval(date('n', $date));
      $event['day'] = intval(date('j', $date));
      $event['time'] = date('g:i A', $date);
      $event['venue'] = myspace_parse_standardize($match['venue']);
      $event['country'] = trim($match['country']);
      $event['state'] = trim($match['state']);
      $event['city'] = trim($match['city']);
      $event['url'] = trim($match['url']);

      if (strpos($event['url'], 'http://www.myspace.com') === FALSE) {
        $event['url'] = 'http://www.myspace.com' . $event['url'];
      } // if

      $events[] = $event;

    } // foreach

  } // elseif

  return $events;

} // myspace_parse_events

/**
 * Returns an array of a single event based on event page HTML.
 */
function myspace_parse_event($html) {

  $event = array('start' => array(), 'end' => array(), 'artist' => array());
  $regex = '#<div class="details vevent[^>]*>[^<]*<h1[^>]*>[^<]*</h1>[^<]*<time class="dtstart[^>]*>(?P<start>[^<]+)</time>((<br>)?[^<]*<time class="dtend[^>]*>(?P<end>[^<]+)</time>)?[^<]*<div class="participantlist">[^<]*<ul>[^<]*<li>[^<]*<ul>[^<]*<li>Artist:[^<]*</li>[^<]*<li><a href="(?P<artisturl>[^"]+)">(?P<artist>[^<]+)</a></li>[^<]*</ul></li>[^<]*</ul>[^<]*</div>[^<]*<h4>(?P<venue>[^<]+)</h4>[^<]*<p class="location[^>]*>(?P<address>.*?)</p>#is';

  if (preg_match($regex, $html, $match)) {

    $start = strtotime(str_replace(' at ', ' ', $match['start']));

    $event['start']['year'] = intval(date('Y', $start));
    $event['start']['month'] = intval(date('n', $start));
    $event['start']['day'] = intval(date('j', $start));
    $event['start']['time'] = date('g:i A', $start);

    if ($match['end']) {

      $end = strtotime(str_replace('at', '', $match['end']));

      $event['end']['year'] = intval(date('Y', $end));
      $event['end']['month'] = intval(date('n', $end));
      $event['end']['day'] = intval(date('j', $end));
      $event['end']['time'] = date('g:i A', $end);

    } // if

    $event['artist']['url'] = trim($match['artisturl']);
    $event['artist']['name'] = myspace_parse_standardize($match['artist']);

    $event['venue'] = trim($match['venue']);
    $event['address'] = trim(str_replace('- Map It', '', myspace_parse_standardize(strip_tags($match['address']))));    

  } // if

  return $event;

} // myspace_parse_event

/**
 * Returns an array of blog post arrays based on blog page HTML.
 */
function myspace_parse_blog($html) {

  $blog = array();
  $regex = '#class="post blogEntry[^>]*>[^<]*<time class="post-date[^>]*>(?P<date>[^<]+)</time>[^<]*<h4 class="post-title[^>]*><a href="[^"]+"[^>]*>(?P<title>[^<]+)</a></h4>[^<]*<article class="post-body[^>]*>(?P<body>.*?)</article>[^<]*<div[^>]*>[^<]*<a href="(?P<url>[^"]+)">(?P<time>[^<]+)</a>#is';

  if (preg_match_all($regex, $html, $matches, PREG_SET_ORDER)) {

    foreach ($matches as $match) {

      $date = strtotime($match['date'] . ' ' . $match['time']);

      $post = array();

      $post['year'] = intval(date('Y', $date));
      $post['month'] = intval(date('n', $date));
      $post['day'] = intval(date('j', $date));
      $post['title'] = trim($match['title']);
      $post['body'] = myspace_parse_standardize($match['body']);
      $post['url'] = 'http://www.myspace.com' . trim($match['url']);
      $post['time'] = date('g:i A', $date);

      $blog[] = $post;

    } // foreach

  } // if

  return $blog;

} // myspace_parse_blog

/**
 * Returns an array of comment arrays based on profile page HTML.
 */
function myspace_parse_comments($html) {

  $comments = array();
  $regex = '#<section class="userComment">[^<]*<div[^>]*>[^<]*<a href="(?P<authorurl>[^"]+)"[^>]*>(?P<author>[^<]+)</a>(?P<body>.*?)</div>[^<]*<ul class="mediaAction">.*?<span class="datePosted" ts="(?P<timestamp>[^"]+)">[^<]*</span>[^<]*<div class="clear"></div>[^<]*</section>#is';

  if (preg_match_all($regex, $html, $matches, PREG_SET_ORDER)) {

    foreach ($matches as $match) {

      // Convert strange MySpace timestamp to standard unix timestamp.

      $date = ($match['timestamp'] / 10000000) - 62135593200;
      
      $comment = array();
      $comment['year'] = intval(date('Y', $date));
      $comment['month'] = intval(date('n', $date));
      $comment['day'] = intval(date('j', $date));
      $comment['time'] = date('g:i A', $date);
      $comment['author'] = array(
        'name' => trim($match['author']),
        'url' => trim($match['authorurl']),
      );
      $comment['body'] = myspace_parse_standardize($match['body']);

      $comments[] = $comment;

    } // foreach

  } // if

  return $comments;

} // myspace_parse_comments

/**
 * Replaces unicode characters with entities or ASCII.
 * Replaces any number of whitespace characters with a single space.
 */
function myspace_parse_standardize($text) {

  $converted = str_replace( 
		array( 
			"\xC2\xA0",
			"\xC3\xA0",
			"\xC3\xA8",
			"\xE2\x80\x9C",
			"\xE2\x80\x9D",
			"\xE2\x80\x99",
		),
		array( 
			' ',
			'&#224;',
			'&#232;',
			'&#8220;',
			'&#8221;',
			'&#8217;',
		),
		$text
	);

  return trim(preg_replace('#\s+#', ' ', $converted));

} // myspace_parse_standardize
